package com.example.akhmadhilmy.submission20

/**
 * Created by Akhmad Hilmy on 9/26/2018.
 */
interface MainView {
    fun showLoading()
    fun hideLoading()
    fun showTeamList(data: List<Team>)


}