package com.example.akhmadhilmy.submission20

import android.view.View

/**
 * Created by Akhmad Hilmy on 9/28/2018.
 */

fun View.visible() {
    visibility = View.VISIBLE
}
fun View.invisible() {
    visibility = View.INVISIBLE
}

