package com.example.akhmadhilmy.submission20

import java.net.URL

/**
 * Created by Akhmad Hilmy on 9/25/2018.
 */
class ApiRepository {
    fun doRequest(url: String): String {
        return URL(url).readText()
    }
}