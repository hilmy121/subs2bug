package com.example.akhmadhilmy.submission20

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.akhmadhilmy.submission20.R.id.team_badge
import com.example.akhmadhilmy.submission20.R.id.team_name
import com.squareup.picasso.Picasso
import org.jetbrains.anko.*

@Suppress("UNREACHABLE_CODE")
/**
 * Created by Akhmad Hilmy on 9/27/2018.
 */
class MainAdapter(private val items : List<Team>) : RecyclerView.Adapter<MainAdapter.TeamViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): TeamViewHolder {
        TODO("not implemented")
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(p0: TeamViewHolder, p1: Int) {
        TODO("not implemented")
    }


    inner class TeamViewHolder(view:View) : RecyclerView.ViewHolder(view) {
        private val teamBadge: ImageView = view.find(team_badge)
        private val teamName: TextView = view.find(team_name)

        fun bindItem (team: Team){
            Picasso.get().load(team.teamBadge).into(teamBadge)
            teamName.text = team.teamName

        }
    }


    class teamUI: AnkoComponent<ViewGroup>{
        override fun createView(ui: AnkoContext<ViewGroup>): View {
            TODO("not implemented")
            return with(ui){
                linearLayout {
                    lparams(width = matchParent, height = wrapContent)
                    padding = dip(16)
                    orientation = LinearLayout.HORIZONTAL

                    imageView {
                        id = R.id.team_badge
                    }.lparams{
                        height = dip(50)
                        width = dip(50)
                    }

                    textView {
                        id = R.id.team_name
                        textSize = 16f
                    }.lparams{
                        margin = dip(15)
                    }

                }
            }

        }


    }


}