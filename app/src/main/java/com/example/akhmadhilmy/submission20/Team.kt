package com.example.akhmadhilmy.submission20

/**
 * Created by Akhmad Hilmy on 9/25/2018.
 */
data class Team (
        @Serializedname("idTeam")
        var teamId: String? = null,

        @SerializedName("strTeam")
        var teamName: String? = null,

        @SerializedName("strTeamBadge")
        var teamBadge: String? = null

)

annotation class Serializedname(val value: String)


annotation class SerializedName(val value: String)

